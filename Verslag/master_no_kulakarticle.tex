\documentclass[a4paper]{article}

%% VERLSAG TYPEN
% Zet elke zin op een nieuwe lijn zodat we gemakkelijk fouten in de code kunnen opsporen
% Als je iets definieert, gebruik \defeq voor :=

\usepackage[dutch]{babel}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{amsmath, amssymb, amsthm}
\usepackage[separate-uncertainty=true]{siunitx}
\usepackage{amsfonts}
\usepackage{subcaption}
\usepackage{float}
\usepackage{booktabs}
\usepackage{parskip}
\usepackage[labelfont=bf]{caption}
\usepackage{systeme}
\usepackage{cmbright}
% \usepackage[left=1.2in,right=0.6in,top=1.5in,bottom=1in]{geometry}
\usepackage{xparse}
\usepackage{transparent}
\usepackage{mathtools}

% appendix
\usepackage[toc,page]{appendix}

% figure support
\usepackage{import}
\usepackage{xifthen}
\usepackage{pdfpages}
\usepackage{transparent}
\newcommand{\incfig}[2][1]{%
    \def\svgwidth{#1\columnwidth}
    \import{./afbeeldingen/}{#2.pdf_tex}
} % laten staan!

\graphicspath{{./afbeeldingen/}}

% om definities aan te geven :=
\makeatletter
\newcommand*{\defeq}{\mathrel{\rlap{%
                     \raisebox{0.3ex}{$\m@th\cdot$}}%
                     \raisebox{-0.3ex}{$\m@th\cdot$}}%
                     =}
\makeatother

\NewDocumentCommand{\INTERVALINNARDS}{ m m }{
    #1 {,} #2
}

\NewDocumentCommand{\interval}{ s m >{\SplitArgument{1}{,}}m m o }{
      \IfBooleanTF{#1}{
          \left#2 \INTERVALINNARDS #3 \right#4
      }{
          \IfValueTF{#5}{
              #5{#2} \INTERVALINNARDS #3 #5{#4}
          }{
              #2 \INTERVALINNARDS #3 #4
          }
      }
}


\title{$\mathcal{PT}$-symmetrische kwantummechanica}
\author{Michiel Debaets, Marie D'haene en Judith Vandewiere}
\date{Academiejaar 2019 -- 2020}
%\address{Bachelorproject fysica}
\begin{document}
\maketitle

%nog een voorwoord waarin we bespreken waarom we dit onderwerp hebben gekozen en dank aan promotor enzo ??

\section{Inleiding}
In de meeste cursussen over kwantummechanica wordt de Hermiticiteit van de Hamiltoniaan (i.e. $\hat{H}^{\dagger} = \hat{H}$) als nodige voorwaarde aangehaald opdat de eigenwaarden ervan re\"eel zijn.
Aangezien de eigenwaarden van $\hat{H}$ corresponderen met de mogelijke energieniveaus van het beschouwde systeem, is dit een voorwaarde om een fysisch relevante theorie te hebben.
Er blijkt echter dat deze eis te streng is: indien $\hat{H}$ \textit{unbroken} $\mathcal{PT}$-symmetrisch (i.e. symmetrisch onder pariteits- en tijdsomkering) is, beschrijft de Hamiltoniaan ook een systeem met re\"ele eigenwaarden.

In dit verslag wordt $\mathcal{PT}$-symmetrische kwantummechanica besproken.
We beginnen met een korte herhaling van de klassieke kwantummechanische postulaten. Daarop volgt een beknopte introductie waarin wordt aangetoond hoe de \textit{unbroken} $\mathcal{PT}$-symmetrie van $\hat{H}$ aanleiding geeft tot re\"ele eigenwaarden.
Daarna wordt een voorbeeld van een niet-Hermitische Hamiltoniaan ($\hat{H} = \hat{p}^2 + \hat{x}^2(i\hat{x})^{\varepsilon}$) uitgewerkt aan de hand van de Wentzel-Kramers-Brioullinmethode (WKB).
Ook klassieke $\mathcal{PT}$-theorie wordt besproken.
Dit komt neer op een deeltje dat onderhevig is aan \textit{complexe krachten}.
Vervolgens overlopen we ook alle benodigdheden voor een werkende theorie van kwantummechanica waarbij er een vergelijking gemaakt wordt tussen klassieke kwantummechanica en de nieuwe $\mathcal{PT}$-symmetrische kwantummechanica.
Uit deze analyse zal blijken dat we de norm op de Hilbertruimte van de toestandsvectoren moeten herdefini\"eren opdat ze positief-definiet zou zijn.
Hierbij wordt de een nieuwe operator $\mathcal{C}$ ingevoerd, gelijkaardig aan ladingstoevoeging uit de klassieke mechanica.
Tot slot worden nog enkele toepassingen besproken.

% \begin{itemize}
%   \item nog praten over de extra $\mathcal{C}$-operator
%   \item overzicht van wat we zullen vertellen
% \end{itemize}

\section{Korte herhaling van klassieke kwantummechanica}
In de klassieke kwantummechanica wordt uitgegaan van drie postulaten
(zie \cite{Bender_2007} en \cite{CursusKwantum}) die hieronder opgelijst staan.
Om de notatie te verlichten, nemen we in wat volgt steeds de conventie aan dat $\hbar \defeq 1$ en $m \defeq \tfrac12$.

\begin{enumerate}
  \item Een systeem wordt volledig beschreven door de bijhorende toestandsvector $\psi(x,t) \in \mathbb{C}$ ook wel golffunctie genaamd.
  Alle toestandsvectoren vormen een Hilbertruimte $\mathcal{H}$ met de positief-definiete Hermitische vorm $\langle \phi,\psi \rangle = \int \overline{\phi(x,t)}\psi(x,t) \: dx$ waarbij de integraal over de volledige ruimte gaat.
  De golffunctie is steeds genormaliseerd, dit wil zeggen $\forall \psi \in \mathcal{H}\text{:} \: \int |\psi(x,t)|^2 \: dx = 1$.
  De golffunctie bevat informatie over de waarschijnlijkheid om het deeltje in een bepaalde omgeving $dx$ rond $x$ terug te vinden, namelijk $P(x,t) \: dx = |\psi(x,t)|^2 \: dx$.

  \item Een observabele $O$ komt overeen met een lineaire, \textit{Hermitische} operator $\hat{O}$ op $\mathcal{H}$ waarvan de eigenwaarden overeenstemmen met de waarden die bij een experiment gemeten kunnen worden.

  \item De Schr\"odingervergelijking $\hat{H}\psi = i \partial_t \psi$ beschrijft hoe een toestandsvector in de tijd verandert.
  De operator $\hat{H}$ wordt de Hamiltoniaan genoemd waarvan de eigenwaarden de mogelijke energieniveaus van het systeem voorstellen.
  Indien het systeem zich in een potentiaal $\hat{V}$ bevindt, is de Hamiltoniaan $\hat{H} = \hat{p}^2 + \hat{V}$.
  Hierin is $\hat{p}$ de impulsoperator en wordt in positierepresentatie geschreven als $i \partial_x$.
\end{enumerate}

Het is dus duidelijk een belangrijke voorwaarde dat de fysisch relevante observabelen Hermitisch zijn.
Dit garandeert immers dat de eigenwaarden ervan re\"eel zijn en dus dat de meetresultaten die we in een experiment kunnen verkrijgen, volgens de kwantummechanica niet complex zijn.
Complexe waarden meten is onmogelijk!

% \begin{itemize}
%   \item postulaten van kwantummechanica
%   \item beetje over Schr\"odingervergelijking
%
% \end{itemize}

% moeten we ook iets zeggen over bounded from below???


\section{Introductie $\mathcal{PT}$-symmetrie} %is extra sectie nodig?
% \begin{itemize}
% 	\item $\hat{\mathcal{P}},\hat{\mathcal{T}}$-operatoren
% 	\item commutatierelaties?
% 	\item broken en unbroken $\mathcal{PT}$-symmetrie
% 	\item bewijsje unbroken $\implies$ reeël spectrum
% \end{itemize}
Zoals eerder vermeld, is Hermiticiteit van $\hat{H}$ een te strenge voorwaarde opdat de energie-eigenwaarden re\"eel zijn.
Hieronder tonen we aan dat unbroken $\mathcal{PT}$-symmetrie hetzelfde resultaat geeft.
Een systeem wordt unbroken $\mathcal{PT}$-symmetrisch genoemd indien de eigenfuncties van de Hamiltoniaan ook eigenfuncties van $\hat{\mathcal{PT}}$ zijn.
Dit betekent dus dat zowel de Schr\"odingervergelijking (de differentiaalvergelijking waaraan de toestandsvectoren voldoen) als de functies zelf aan de symmetrie voldoen.
We spreken van \textit{broken} symmetrie wanneer de differentiaalvergelijking symmetrisch is, maar de bijhorende functies dat niet zijn.
Een voorbeeld waarin de tijdsomkeringssymmetrie gebroken wordt, is bij de vergelijking $\ddot{y}(t) = y(t)$.
Deze is symmetrisch onder $t \to -t$, maar de oplossingen $e^{-t}$ en $e^{t}$ zijn dat niet \cite{Bender_2005}.

Nu geven we een kort overzicht van hoe de $\hat{\mathcal{P}}$- en $\hat{\mathcal{T}}$-operator de golffunctie transformeren en wat $\mathcal{PT}$-symmetrie precies inhoudt.

% We zullen aantonen dat we de voorwaarde om een reeël spectrum van een Hamiltoniaan te garanderen namelijk Hermiticiteit van die Hamiltoniaan, te vervangen is door de voorwaarde van $\mathcal{PT}$-symmetrie (parity-time symmetry).
Een Hamiltoniaan $\hat{H}$ is $\mathcal{PT}$-symmetrisch als
\begin{equation}
	\hat{H} = \hat{H}^{\mathcal{PT}} \defeq \left(\hat{\mathcal{PT}}\right)\hat{H}\left(\hat{\mathcal{PT}}\right)
\end{equation}
waarbij pariteitsoperator $\hat{\mathcal{P}}$ en tijdsomkeringsoperator $\hat{\mathcal{T}}$ in positierepresentatie als volgt gedefinieerd zijn
\begin{equation}
  \begin{aligned}
  	\mathcal{\hat{P}}: x \rightarrow -x,\;\; p \rightarrow -p, \\
  	\mathcal{\hat{T}}: x \rightarrow x,\;\; p \rightarrow -p,\;\; i \rightarrow -i.
  \end{aligned}
\end{equation}
De pariteitsoperator is dus een lineaire operator terwijl de tijdsomkeringsoperator anti-lineair is: bij transformatie wordt het teken van de imaginaire eenheid $i$ omgekeerd.

De voorwaarde dat $\hat{H}$ symmetrisch is onder pariteits- en tijdsomkering is echter nog niet voldoende om een re\"eel spectrum te verkrijgen.
Er is immers geen garantie dat de eigenfuncties van $\hat{H}$ ook eigenfuncties van $\hat{\mathcal{PT}}$ zijn.
Indien dit het geval is, is de symmetrie \textit{unbroken}, dus $\hat{H}\psi = E\psi \Rightarrow \hat{\mathcal{PT}}\psi = \lambda\psi$ met $\lambda \in \mathbb{C}$.
% In het andere geval, spreken we van \textit{broken} $\mathcal{PT}$-symmetrie \cite{Bender_2007}.
% Merk op dat dit ligt aan het feit dat symmetrie\"en van de Schr\"odingervergelijking en bijhorende randvoorwaarden niet altijd overgenomen wordt in de eigenfuncties.
% Indien een oplossing van de Schr\"odingervergelijking dus $\mathcal{PT}$-symmetrisch is, is die ook een eigenfunctie van $\hat{\mathcal{PT}}$ en is de symmetrie unbroken \cite{Bender_2005}.

Nu zullen we aantonen dat unbroken $\mathcal{PT}$-symmetrie aanleiding geeft tot een re\"eel spectrum.
Veronderstel dat $\hat{H}\psi = E\psi$ en $\hat{\mathcal{PT}}\psi = \lambda\psi$.
Wanneer we de laatste vergelijking links met $\hat{\mathcal{PT}}$ vermenigvuldigen, krijgen we
\begin{equation}
  \begin{aligned}
    \left( \hat{\mathcal{PT}} \right)\left( \hat{\mathcal{PT}} \right)\psi &= \left( \hat{\mathcal{PT}} \right)\lambda\psi \\
    \left( \hat{\mathcal{P}}^2 \hat{\mathcal{T}}^2 \right)\psi &= \overline{\lambda}\left(\hat{\mathcal{PT}} \right)\psi \\
    \psi &= \overline{\lambda}\lambda\psi = |\lambda|^2 \psi
  \end{aligned}
\end{equation}
en aangezien we uitgaan van niet-triviale eigenvectoren, volgt hieruit dat $|\lambda|^2 = 1$ of dus $\lambda = e^{i\alpha}$ met $\alpha \in \interval[{0,2\pi})$.
Omdat de kwantummechanica invariant is onder vermenigvuldiging van de golffunctie met een fasefactor (verwachtingswaarden blijven hetzelfde) kunnen we nu de herschaalde golffunctie invoeren $\phi \defeq \exp\left(\frac{i\alpha}{2}\right)\psi$ zo dat de bijhorende eigenwaarde van $\hat{\mathcal{PT}}$ 1 is \cite{CursusKwantum}.
Inderdaad,
\begin{equation}
  \left( \hat{\mathcal{PT}} \right)\phi = \left( \hat{\mathcal{PT}} \right)e^{\tfrac{i\alpha}{2}}\psi = e^{-\tfrac{i\alpha}{2}} e^{i\alpha} \psi = e^{\tfrac{i\alpha}{2}}\psi = \phi.
\end{equation}
Omdat $\hat{H}$ een lineaire operator is, geldt nog steeds $\hat{H}\phi = E\phi$.
Vermenigvuldigen we nu in deze vergelijking beide leden links met $\hat{\mathcal{PT}}$, dan:
\begin{equation}
  \begin{aligned}
    \left( \hat{\mathcal{PT}} \right) \hat{H} \: \phi &= \left( \hat{\mathcal{PT}} \right) E \: \phi \\
    \hat{H} \left( \hat{\mathcal{PT}} \right) \phi &= \overline{E} \phi \\
    E \phi &= \overline{E} \phi
  \end{aligned}
\end{equation}
Omdat we veronderstellen $\phi \neq 0$, moet $\overline{E} = E$ en dus $E \in \mathbb{R}$.
Dit is echter nog geen bewijs dat we een goede theorie van kwantummechanica kunnen opbouwen aan de hand van $\mathcal{PT}$-symmetrische operatoren.
We hebben immers nog geen positief-definiete Hermitische vorm gevonden voor de Hilbertruimte.
Dit komt aan bod in een later onderdeel. NOG VERWIJZEN!

\section{De WKB-methode}
 Om de golffuncties te vinden, moeten we de Schrödingervergelijking oplossen.
 Dit is een lineaire differentiaalvergelijking, maar na invullen van een ansatz voor de golffunctie verandert deze in een niet-lineaire differentiaalvergelijking.
 Veelal bestaat er geen analytische oplossing van dergelijke differentiaalvergelijkingen en daarom gebruiken we in deze sectie de Wentzel-Kramers-Brillouinbenadering (WKB).

 Deze benadering kan gebruikt worden wanneer de potentiaal $V(x)$ traag verandert tegenover de positie $x$ \cite{Bransden}.
 Dit betekent dus dat $\frac{dV(x)}{dx}$ voldoende klein moet zijn.
 We veronderstellen in deze sectie een deeltje met eendimensionale beweging en vaste massa $m$ in een potentiaal $V(x)$, met bijbehorende tijdsonafhankelijke Schrödingervergelijking
\begin{equation}
	-	 \frac{\mathrm{d}^{2} \psi(x)}{\mathrm{d} x^{2}}+V(x) \psi(x)=E \psi(x).
	\label{WKB_schrodinger_tonafh}
\end{equation}

	Als de potentiaal constant is of $V(x) = V_{0}$ dan zijn de oplossingen van \eqref{WKB_schrodinger_tonafh} vlakke golven

\begin{equation}
	\frac{\mathrm{d}^{2} \psi(x)}{\mathrm{d} x^{2}}= p_{0} \psi(x)
	\Leftrightarrow
	\psi(x)=A \exp \left(\pm \frac{\mathrm{i}}{\hbar} p_{0} x\right)
\end{equation}
 met
\begin{equation}
	p_{0} = \sqrt{2m(E-V_{0})}
\end{equation}
en $A$ een constante.

\subsection{Standaard QM benadering}
Geïnspireerd op de oplossingen van vlakke golven, het klassieke geval, zoeken we oplossingen van de vorm

\begin{equation}
	\psi(x)=Ae^{\frac{\mathrm{i}}{\hbar} S(x)}
	\label{WKB_anzats}
\end{equation}

met een potentiaal dat 'traag varieert'.
Dit betekent dat $V(x)$ weinig verandert over de de Broglie golflengte

\begin{equation}
	\lambda(x)= \frac{h}{p(x)}.
\end{equation}

M.a.w. we proberen de oplossingen van vlakke golven (met constante potentiaal) te extrapoleren naar oplossingen voor traag variërende potentialen. 
Geassocieerd met een deeltje dat een energie groter dan de potentiaal heeft ($E>V$), waarbij
\begin{equation}
	\label{WKB_klassieke_p}
	p(x) = \sqrt{2m(E-V(x))}\in \mathbb{R}
\end{equation}
het klassieke momentum op positie x is.
Concreet betekent dit dat $V(x)-V(x+\lambda(x))$ ongeveer nul is.
%De approximatie dat $V(x)$ traag varieert tegenover de golflengte geldt als de golflengte klein is.%
In het klassieke limiet is $\lambda$, de de Broglie golflengte, nul waardoor de potentiaal altijd traag variereert tegenover $\lambda$ en WKB geldig is. Daarom wordt de WKB-methode vaak een semi-klassieke benadering genoemd, een randgeval tussen klassieke en quantum mechanica.
\noindent
Als we \eqref{WKB_anzats} invullen in de Schrödingervergelijking \eqref{WKB_schrodinger_tonafh}, verkrijgen we

\begin{equation}
	-\frac{i \hbar}{2 m} \frac{\mathrm{d}^{2} S(x)}{\mathrm{d} x^{2}}+\frac{1}{2 m}\left[\frac{\mathrm{d} S(x)}{\mathrm{d} x}\right]^{2}+V(x)-E=0.
	\label{WKB_schrodinger}
\end{equation}

Tot zover zijn er nog geen approximaties gemaakt. De differentiaalvergelijking \eqref{WKB_schrodinger} is niet-lineair en er bestaat geen analytische oplossing voor. We moeten dus de oplossing van \eqref{WKB_schrodinger} benaderen.
Merk op dat de linkerterm van \eqref{WKB_schrodinger} wegvalt wanneer we vlakke golven hebben ($S(x) = \pm p_{0}x $). Die linkerterm is proportioneel met $\hbar$ waardoor die in het klassieke geval ($\lambda\rightarrow0$) wegvalt. Als gevolg is die linkerterm een maat voor hoe ver we van de constante potentiaal en klassieke mechanica verwijderd zijn. Dit suggereert dat we $\hbar$ als parameter van kleinheid kunnen beschouwen en $S(x)$ expanderen in de volgende machtreeks:

\begin{equation}
	\label{WKB_S(x)_expansie}
	S(x)=S_{0}(x)+\hbar S_{1}(x)+\frac{\hbar^{2}}{2} S_{2}(x)+\cdots
\end{equation}

Laten we nu de expansie van $S(x)$ \eqref{WKB_S(x)_expansie} invoegen in \eqref{WKB_schrodinger} en in de eerste 3 machten van $\hbar$ splitsen

\begin{equation}
\begin{aligned}
\hbar^0(\frac{1}{2 m}\left[\frac{\mathrm{d} S_{0}(x)}{\mathrm{d} x}\right]^{2}+V(x)-E) &=0 \\
\hbar^1(\frac{\mathrm{d} S_{0}(x)}{\mathrm{d} x} \frac{\mathrm{d} S_{1}(x)}{\mathrm{d} x}-\frac{\mathrm{i}}{2} \frac{\mathrm{d}^{2} S_{0}(x)}{\mathrm{d} x^{2}}) &=0 \\
\hbar^2(\frac{\mathrm{d} S_{0}(x)}{\mathrm{d} x} \frac{\mathrm{d} S_{2}(x)}{\mathrm{d} x}+\left[\frac{\mathrm{d} S_{1}(x)}{\mathrm{d} x}\right]^{2}-\mathrm{i} \frac{\mathrm{d}^{2} S_{1}(x)}{\mathrm{d} x^{2}}) &=0
\\
\centering\cdots
\end{aligned}
\end{equation}

met deze vergelijking kunnen we $S_{0}(x),S_{1}(x),S_{2}(x),...$ vinden. 

\subsubsection*{Nulde orde}
Op nulde orde vinden we bij een deeltje met energie E dat
\begin{equation}
	\label{WKB_nuldeorde}
	\begin{aligned}
	\frac{\mathrm{d} S_{0}(x,E)}{\mathrm{d} x} &= \sqrt{2m(E-V(x))} \\
	\Rightarrow S_{0}(x,E)&=\pm \int p\left(x\right) \mathrm{d} x
	\end{aligned}
\end{equation}
hierbij is de integratieconstante geabsorbeerd in \eqref{WKB_anzats} en $p(x)\in \mathbb{C}$. Want er is nog geen onderscheid gemaakt tussen $E>V(X)$ en $E<V(X)$ respectievelijk de klassieke zone en de "verboden" zone. Merk op dat $S_{0}(x)$ het klassieke limiet geeft van $S(x)$ ($\hbar=0$). %%eventueel opemrken dat die integraal de actie is want E is cnst over traject van deeltje %%


\subsubsection*{Eerste orde}
Op eerste orde vinden we bij een deeltje met energie E dat, gebruik makend van \eqref{WKB_nuldeorde},
\begin{equation}
	\label{WKB_eersteorde}
	\begin{aligned}
	2p(x)\frac{\mathrm{d} S_{1}(x,E)}{\mathrm{d} x} -i\frac{\mathrm{d}  p(x)}{\mathrm{d} x} &= 0 \\
	\Rightarrow S_{1}(x,E) =\frac{i}{2} \int \frac{\mathrm{d} p(x)}{p(x)}&=\frac{i}{2}\log(p(x))
	\end{aligned}
\end{equation}
waarbij de integratieconstante opnieuw geabsorbeerd is en $p(x)\in \mathbb{C}$.

\subsubsection*{Tweede orde}
De tweede orde is meer werk maar het resultaat is de moeite waard. We vinden bij een deeltje met energie E gebruik makend van \eqref{WKB_nuldeorde} en \eqref{WKB_eersteorde} dat
\begin{equation}
	\begin{aligned}
	p(x)\frac{\mathrm{d} S_{2}(x,E)}{\mathrm{d} x} -\frac{3}{4p^2(x)}\left[\frac{\mathrm{d}  p(x)}{\mathrm{d} x}\right]^2 +\frac{1}{2p(x)}\frac{\mathrm{d^2}  p(x)}{\mathrm{d^2} x} &= 0 \\
		\Updownarrow \text{Stel} \ \ p(x)\ne0 \quad \quad \quad & \\
	\frac{\mathrm{d} S_{2}(x,E)}{\mathrm{d} x} = \frac{3}{4p^3(x)}\left[\frac{\mathrm{d}  p(x)}{\mathrm{d} x}\right]^2 -\frac{1}{2p^2(x)}\frac{\mathrm{d^2}  p(x)}{\mathrm{d^2} x}.&
	\end{aligned}
\end{equation}
Veronderstellen dat $p(x)\ne0$ is geen probleem omdat het anzats van WKB, traag variërende potentiaal t.o.v de de Brogelie golflengte ($\lambda \rightarrow \infty$), niet geldig is. De volgende stappen om $S_{2}(x)$ te berekenen zijn langdradig maar triviaal. Als eindresultaat is de tweede orde de volgende uitdrukking

\begin{equation}
	\label{WKB_tweedeeorde}
	S_{2}(x,E)=\frac{1}{2} m[p(x)]^{-3} \frac{\mathrm{d} V(x)}{\mathrm{d} x}-\frac{1}{4} m^{2} \int\left[p\left(x\right)\right]^{-5}\left[\frac{\mathrm{d} V\left(x\right)}{\mathrm{d} x}\right]^{2} \mathrm{d} x.
\end{equation}

Uit de uitdrukking van $S_{2}(x,E)$ volgt dat deze klein is wanneer $\frac{\mathrm{d} V(x)}{\mathrm{d} x}$ klein is en $p(x)$ niet dichtbij nul is. \textcolor{red}{In de paragraaf (referentie, moet nog gemaakt worden) tonen we het expiciet verband aan tussen verwaarloosbare $S_{2}(x)$ en traag variërende potentiaal, in feite zijn het dezelfde voorwaarden.} Als de hogere afgeleiden van $V(x)$ ook klein zijn zal $S_{3}(x,E),S_{4}(x,E),\cdots$ ook verwaarloosbaar zijn.


Als $V(x)$ een traag variërende potentiaal en $p(x)$ niet dichtbij nul is, kan je enkel de eerste twee termen, $S_{0}(x,E)$ en $S_{1}(x,E)$, overhouden. Als we de termen \eqref{WKB_nuldeorde} en \eqref{WKB_eersteorde} invullen in \eqref{WKB_anzats} verkrijgen we de WKB approximatie namelijk

\begin{equation}
\label{WKB_golffunctie}
	\begin{aligned}
	\psi(x)=&[p(x)]^{-1 / 2}\left\{A \exp \left[\frac{\mathrm{i}}{\hbar} \int p(x) \mathrm{d} x\right]\right.\\
	&\left.+B \exp \left[-\frac{\mathrm{i}}{\hbar} \int p(x) \mathrm{d} x\right]\right\}
	\end{aligned}
\end{equation}
met A en B reëele constantes. Waarbij het teken de richting van propagatie is.


Laten we de WKB oplossing \eqref{WKB_golffunctie} bekijken in het klassieke en verboden gebied, resp. $E>V$ en $E<V$. Stel dat op de positie $x_{1}$ de potentiaal even groot is als de energie ($E=V(x_{1})$). \textcolor{red}{figuurtje met potentiaal en turning point, $x^{1}$}! Waarbij we even opmerken dat $p(x)$ reëel is in de klassieke zone en puur imaginair in de verboden zone. Dan verkrijgen we de volgende goffuncties:

\begin{equation}
\label{WKB_golffunctie_splitsing}
	\begin{align}
	\psi(x)=A|p(x)|^{-1 / 2} \exp \left[\pm \frac{1}{\hbar} \int^{x^{1}}\left|p\left(x\right)\right| \mathrm{d} x\right], &\quad E<V \\
	\psi(x)=A[p(x)]^{-1 / 2} \exp \left[\pm \frac{\mathrm{i}}{\hbar} \int_{x^{1}} p\left(x\right) \mathrm{d} x\right], &\quad E>V
	\end{align}
\end{equation}

waarbij A een reëele constante is. We herkennen hierin de typische exponentieel dalende functie in de verboden zone en goniometrische functie in de klassieke zone. Deze oplossingen zijn accuraat wanneer het criterium (\textcolor{red}{eqref naar paragraaf}) voldaan is, wat equivalent is met een traag variërende potentiaal waarbij $V-E$ groot genoeg is.

\subsubsection{Verbindingsformule}
Zoals eerder al vermeld is WKB niet geldig in de buurt van $E=V(x)$ ($p(x)=0$) (\textcolor{red}{eqref naar paragraaf}) omdat $\lambda$ dan naar oneindig gaat, bijgevolg is de enige traag variërende potentiaal een constante. 
M.a.w. WKB is niet geldig in een keerpunt. 
Denk aan een veer met bepaald uitwijking de keerpunten zijn de posities wanneer zijn snelheid nul is enkele momentel later keert ook zijn richting van snelheid. 
We zeggen dat WKB golfuncties in \eqref{WKB_golffunctie_splitsing} asymptotisch geldig zijn, want ze mogen alleen verweg van het meest naburige keerpunt gebruikt worden. 
Iets wat we in paragraaf \ref{harmosc} expliciet zullen gebruiken. 

Omdat de exacte golffunctie continu en afleidbaar moet zijn voor alle x, zou het mogelijk moeten zijn om een verbindingsformule tussen de twee oplossingen van WKB kan verbinden langs het keerpunt. In deze paragraaf gaan we hier dieper op in. Er zijn meerdere mogelijkheden maar hier beschrijven we de 'uniforme benadering'. 

Om de oplossing rond het keerpunt te bepalen zullen we de Schrödinger vergelijking met een benadering (die geldig is dichtbij een keerpunt) omvormen tot de Airy vergelijking. De Airy vergelijking is de simpelste 2de graads vergelijking met een keerpunt en een analytische oplossing,

\begin{equation}
\label{WKB_Airy_div}
\frac{d^{2} y}{d x^{2}}-x y=0.
\end{equation}

Laten we eerst de Schrödingervergelijking omvoren tot een Airy functie voordat we de oplossing van de Airy differentiaalvergelijking bekijken. We beschouwen hier een potentiaal met barrière aan de linkerkant en een keerpunt op $x=x_{1}$(\textcolor{red}{figuur}). Bijgevolg is het locale klassieke momentum \eqref{WKB_klassieke_p} nul wanneer $x=x_{1}$. Voor $x<x_{1}$ is $p^{2}(x)$ negatief en voor $x>x_{1}$ is $p^{2}(x)$ positief. Als $x$ dicht genoeg bij $x_{1}$ ligt, kunnen we $p^{2}(x)$ bendaren met een taylorexpansie tot op eerste orde,

\begin{equation}
\label{WKB_connectie_pp}
p^{2}(x) \simeq A\left(x-x_{1}\right). \quad A=\left[\frac{\mathrm{d} p^{2}(x)}{\mathrm{d} x}\right]_{x=x_{1}}
\end{equation}

Waarbij A een strikt positieve constante is.

Gebruikmakend van \eqref{WKB_connectie_pp}, kan de Schrödingervergelijking

\begin{equation}
\left[\frac{\mathrm{d}^{2}}{\mathrm{d} x^{2}}+\hbar^{-2} p^{2}(x)\right] \psi(x)=0
\end{equation}

in de buurt van het keerpunt omgevormd worden tot de vergelijking

\begin{equation}
\left[\frac{\mathrm{d}^{2}}{\mathrm{d} x^{2}}+A \hbar^{-2}\left(x-x_{1}\right)\right] \psi(x)=0
\end{equation}


\section{Een $\mathcal{PT}$-symmetrische Hamiltoniaan met re\"eel spectrum}\label{harmosc}

In deze sectie zullen we het spectrum bepalen van de eendimensionale Hamiltoniaan
\begin{equation}
  \hat{H} = \hat{p}^2 + \hat{x}^2 (i\hat{x})^{\varepsilon}.
  \label{hamiltonian}
\end{equation}

Voor $\varepsilon > 0$ zijn alle eigenwaarden re\"eel.
Merk op dat dit inderdaad te verwachten is voor $\varepsilon \in \{ 0, 4, 8 \ldots \}$, omdat de Hamiltoniaan dan van de vorm $\hat{p}^2 + \hat{x}^{2+4n} \: (n\in\mathbb{N})$ is en dus Hermitisch \'en naar onder begrensd is (dit is door het plusteken voor de potentiaal).
Hieronder zullen we echter aantonen dat er ook een re\"eel spectrum is voor $\varepsilon \in \{ 2, 6, 10 \ldots \}$, waarvoor we hebben dat $\hat{H} = \hat{p}^2 - \hat{x}^{2+2n} \: (n \in \mathbb{N} \text{ oneven})$.
De potentiaal komt dus voor met een minteken, waardoor de eigenwaarden niet naar onder begrensd zijn \cite{Schmidt_2013}.
Daarnaast verkrijgen we ook voor $\varepsilon$ oneven, waarbij $\hat{H} = \hat{p}^2 \pm i\hat{x}^{2+n} \: (n \in \mathbb{N} \text{ oneven})$, enkel re\"ele energie-eigenwaarden.

% Note that in general $\hat{H}$ is non-Hermitian, only for $\varepsilon$ even.
% A further distinction is made between $\varepsilon \in \{ 0, 4, 8 \ldots \}$ and $\varepsilon \in \{ 2, 6, 10 \ldots \}$.
% In the first case we obtain a Hamiltonian of the form $\hat{p}^2 + \hat{x}^{2+4n} \: (n\in\mathbb{N})$ for which the set of energy eigenvalues is bounded below \cite{Schmidt_2013}.
% Note that when $n=0$ we get the Hamiltonian that describes the harmonic oscillator.

Om de energie-eigenwaarden van $\hat{H}$ te vinden, lossen we de tijdsonafhankelijke Schr\"odingervergelijking op:
\begin{equation}
  \hat{H}\psi = E\psi.
\end{equation}
In de positierepresentatie vervangen we $\hat{x} \to x \in\mathbb{C}$ (WAAROM) en $\hat{p} \to -i \frac{d}{dx}$ waardoor we een tweede-orde gewone differentiaalvergelijking verkrijgen van de vorm
\begin{equation}
  -\frac{d^2\psi(x)}{dx^2} + x^2 (ix)^{\varepsilon} \: \psi(x) = E\psi(x).
\end{equation}
Deze kan niet exact opgelost worden voor alle waarden van $\varepsilon$ \cite{Bender_2007}.
Daarom beschouwen we eerst het gedrag van $\psi(x)$ voor $|x| \to \infty$ aan de hand van de Wentzel-Kramers-Brillouin-benadering (WKB).
Onder bepaalde voorwaarden (NOG AANVULLEN) stelt deze dat we voor zeer grote waarden van $|x|$ de golffunctie kunnen benaderen als volgt:
\begin{equation}
  \psi_{\pm}(x) \propto \exp\ \left( \pm \int \sqrt{V(x)} \: dx \right).
  \label{golffunctie}
\end{equation}
De volledige toestandsvector voor $|x|\to\infty$ is steeds een lineaire combinatie van deze twee oplossingen $\psi(x) = A\psi_-(x) + B\psi_+(x)$ waarbij $A,B \in \mathbb{C}$.
De constanten bepalen we door randvoorwaarden op te leggen, namelijk $\psi(x)\to 0$ voor $|x|\to\infty$ en $\int|\psi(x)|^2 \: dx = 1$.

In het concrete geval (\ref{hamiltonian}) bepalen we dus eerst
\begin{align*}
  \int\sqrt{x^2(ix)^{\varepsilon}} \: dx
  &= i^{\frac{\varepsilon}{2}} \int x^{\frac{2+\varepsilon}{2}} \: dx \\
  &= i^{\frac{\varepsilon}{2}} \left( \frac{2x^{\frac{4+\varepsilon}{2}}}{4+\varepsilon} + C \right)
\end{align*}
waarbij $C$ een integratieconstante is.
% die we weglaten in verdere uitwerkingen omdat we de golffunctie toch maar beschouwen op vermenigvuldiging met een constante na.
Dit resultaat substitueren we nu in vergelijking (\ref{golffunctie}) en schrijven $x \in \mathbb{C}$ als $x = r e^{i\theta}$ met $\theta \in \interval[{0,2\pi})$ en $i = e^{\frac{i\pi}{2}}$.
Zo verkrijgen we
\begin{align*}
  \psi\left( re^{i\theta} \right)
  &\propto \exp\left( \pm \left(e^{\frac{i\pi}{2}}\right)^{\frac{\varepsilon}{2}} \frac{2\left(r e^{i\theta}\right)^{\frac{4+\varepsilon}{2}}}{4+\varepsilon} \right) \exp\left( i^{\frac{\varepsilon}{2}} C \right) \\
  % &\propto \exp\left( \pm i^{\frac{\varepsilon}{2}} \frac{2\left(r e^{i\theta}\right)^{\frac{4+\varepsilon}{2}}}{4+\varepsilon} \right) \\
  &\propto \exp\left( \mp e^{i\pi} \frac{2}{4+\varepsilon} r^{\frac{4+\varepsilon}{2}} e^{\frac{i\pi\varepsilon}{4}} e^{\frac{i\theta(4+\varepsilon)}{2}} \right) \\
  &\propto \exp\left( \mp \frac{2}{4+\varepsilon} r^{\frac{4+\varepsilon}{2}} e^{\frac{i(\varepsilon+4)}{2} \left( \frac{\pi}{2} + \theta \right)} \right).
\end{align*}
Nu we weten hoe de golffunctie eruit ziet voor $|x| \to \infty$, kunnen we de \textit{Stokes} en \textit{anti-Stokes lines} bepalen.
Dit zijn rechten in het complexe vlak waarlangs $\psi(x)$ een puur oscillerende functie van $x$ is, respectievelijk een pure demping is \cite{Schmidt_2013}.
Om dus de Stokes lines te vinden, moeten we in $\psi\left( re^{i\theta} \right)$ eisen dat het re\"ele gedeelte verdwijnt.
Hiervoor schrijven we eerst
\begin{equation}
  e^{\frac{i(\varepsilon+4)}{2} \left( \frac{\pi}{2} + \theta \right)} = \cos\left( \frac{\varepsilon+4}{2} \left( \frac{\pi}{2} + \theta \right) \right) + i \sin\left( \frac{\varepsilon+4}{2} \left( \frac{\pi}{2} + \theta \right) \right).
  \label{cos_sin_relatie}
\end{equation}
Hieruit krijgen we de eenvoudige voorwaarde dat $\operatorname{Re}(\psi)$ verdwijnt voor
\begin{equation}
  \cos\left( \frac{\varepsilon+4}{2} \left( \frac{\pi}{2} + \theta \right) \right) = 0
\end{equation}
waaruit de hoeken van de Stokes lines verkrijgen
\begin{equation}
  \theta_{\text{S}} = \frac{2\pi}{\varepsilon+4} \left( k + \frac12 \right) - \frac{\pi}{2} \quad \left(k \in \mathbb{Z}\right).
\end{equation}
Op analoge wijze bekomen we de hoeken van de anti-Stokes lines door de sinus in (\ref{cos_sin_relatie}) nul te stellen:
\begin{equation}
  \theta_{\text{A}} = \frac{2k\pi}{\varepsilon + 4} - \frac{\pi}{2} \quad \left( k \in \mathbb{Z} \right).
\end{equation}

% figuren Stokes
\begin{figure}[ht]
  \begin{subfigure}{0.5\textwidth}
    \centering
    \incfig[0.8]{eps1}
    \caption{$\varepsilon = 1$}
    \label{fig:eps1}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
      \centering
      \incfig[0.8]{eps3}
      \caption{$\varepsilon=3$}
      \label{fig:eps3}
  \end{subfigure}
\end{figure}

\begin{figure}[ht]
  \begin{subfigure}{0.5\textwidth}
    \centering
    \incfig[0.8]{eps2}
    \caption{$\varepsilon = 2$}
    \label{fig:eps2}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
      \incfig[0.8]{eps6}
      \caption{$\varepsilon=6$}
      \label{fig:eps6}
  \end{subfigure}
\end{figure}

\begin{figure}[ht]
  \begin{subfigure}{0.5\textwidth}
    \centering
    \incfig[1]{eps4}
    \caption{$\varepsilon = 4$}
    \label{fig:eps4}
  \end{subfigure}
  \begin{subfigure}{0.5\textwidth}
    \centering
    \incfig{eps8}
    \caption{$\varepsilon = 8$}
    \label{fig:eps8}
\end{subfigure}

\end{figure}
%

Nu rest ons nog de energie-eigenwaarden te bepalen bij de golffunctie.
Volgens de WKB-kwantisatievoorwaarde (VERWIJZEN NAAR BRON) moet
\begin{equation}
  \left( n + \frac12 \right) \pi = \int_{x_-}^{x_+} \sqrt{E_n - V(x)} \: dx \quad \left(n \in \mathbb{N}\right)
  \label{kwantisatievoorwaarde}
\end{equation}
waarbij $x_-$ en $x_+$ de \textit{turning points} zijn van het systeem.
Dit zijn de punten waar de totale energie gelijk is aan de potentiaal: $E_n = V(x)$.
In het geval dat $V(x) = -(ix)^{2+\varepsilon} = -\left(ire^{i\theta}\right)^{2+\varepsilon}$ krijgen we dat zowel $-\left( ie^{i\theta} \right)^{2+\varepsilon} = 1$ als $r^{2+\varepsilon} = E_n$.
Hieruit volgt dan dat
\begin{equation}
  x_+ = E_n^{\frac{1}{2+\varepsilon}} e^{i\gamma} \qquad x_- = E_n^{\frac{1}{2+\varepsilon}} e^{i\zeta}
\end{equation}
waarbij $\gamma,\zeta \in \interval[{0,2\pi})$ hoeken zijn waaronder de golffunctie naar nul gaat op oneindig.

Met bovenstaande voorwaarden kan de integraal in (\ref{kwantisatievoorwaarde}) opgelost worden als volgt
\begin{align*}
  \int_{x_-}^{x_+} \sqrt{E_n + (ix)^{2+\varepsilon}} \: dx
  &= \int_{x_-}^{0} \sqrt{E_n + (ix)^{2+\varepsilon}} \: dx + \int_{0}^{x_+} \sqrt{E_n + (ix)^{2+\varepsilon}} \: dx \\
\end{align*}
Indien we de substitutie doorvoeren $y = \frac{x}{x_-}$ zo dat $dx = x_- dy$ wordt in de eerste integraal en $y = \frac{x}{x_+}$ zo dat $dx = x_+ dy$ in de tweede integraal, dan verkrijgen we:

\begin{align*}
  \int_{x_-}^{x_+} \sqrt{E_n + (ix)^{2+\varepsilon}} \: dx
  &= x_- \int_{1}^{0} \sqrt{E_n + (iyx_-)^{2+\varepsilon}} \: dy + x_+ \int_{0}^{1} \sqrt{E_n + (iyx_+)^{2+\varepsilon}} \: dy \\
  &= E_n^{\frac{1}{2+\varepsilon}} e^{i\zeta} \int_{1}^{0} \sqrt{E_n + (iyE_n^{\frac{1}{2+\varepsilon}} e^{i\zeta})^{2+\varepsilon}} \: dy \\
  &\hspace{4em} + E_n^{\frac{1}{2+\varepsilon}} e^{i\gamma} \int_{0}^{1} \sqrt{E_n + (iyE_n^{\frac{1}{2+\varepsilon}})^{2+\varepsilon}} \: dy \\
  &= E_n^{\frac{4+\varepsilon}{4+2\varepsilon}} e^{i\zeta} \int_{1}^{0} \sqrt{1 + (iy e^{i\zeta})^{2+\varepsilon}} \: dy
  + E_n^{\frac{4+\varepsilon}{4+2\varepsilon}} e^{i\gamma} \int_{0}^{1} \sqrt{1 + (iy e^{i\gamma})^{2+\varepsilon}} \: dy \\
  &= - E_n^{\frac{4+\varepsilon}{4+2\varepsilon}} e^{i\zeta} \int_{0}^{1} \sqrt{1 - y^{2+\varepsilon}} \: dy
  + E_n^{\frac{4+\varepsilon}{4+2\varepsilon}} e^{i\gamma} \int_{0}^{1} \sqrt{1 - y^{2+\varepsilon}} \: dy \\
  &= E_n^{\frac{4+\varepsilon}{4+2\varepsilon}} \left( e^{i\gamma} - e^{i\zeta} \right) \int_{0}^{1} \sqrt{1 - y^{2+\varepsilon}} \: dy \\
  &= \left( n + \frac12 \right) \pi \quad \left( n \in \mathbb{N} \right)
\end{align*}
De laatste uitdrukking is re\"eel.
Merk op dat $\int_{0}^{1} \sqrt{1 - y^{2+\varepsilon}} \: dy$ re\"eel is.
Daarom moet $\left( e^{i\gamma} - e^{i\zeta} \right) \in \mathbb{R}$ opdat de laatste gelijkheid zin heeft.
Dit is het geval indien $\zeta = \pi - \gamma$ en op deze manier bekomen we dat
\begin{equation}
  \left( n + \frac12 \right) \pi = E_n^{\frac{4+\varepsilon}{4+2\varepsilon}} 2\cos\gamma \int_{0}^{1} \sqrt{1 - y^{2+\varepsilon}} \: dy.
  \label{eigenvals}
\end{equation}
De integraal in (\ref{eigenvals}) bepaalden we met behulp van \verb|Mathematica| aangezien de uitwerking ervan geen meerwaarde geeft aan dit verslag en zo bekomen we ten slotte
\begin{equation}
  E_n = \left( \frac{\Gamma\left( \frac32 + \frac{1}{2+\varepsilon}\right)\left( n + \frac12 \right)\sqrt{\pi}}{\Gamma(1 + \frac{1}{2+\varepsilon}) \cos\gamma} \right)^{\frac{2\varepsilon + 4}{\varepsilon + 4}}
\end{equation}
Bijgevolg zijn de eigenwaarden van deze klasse van Hamiltonianen re\"eel voor waarden van $\varepsilon > 0$.

\section{$\mathcal{PT}$-symmetrische klassieke mechanica}

\section{$\mathcal{PT}$-symmetrische kwantummechanica}
%C-operator

\section{Toepassingen}

\newpage
\appendix
	\section{Airy functies}
	In deze bijlage gaan we de oplossingen van de airy differentiaalvergelijking,
	
	\begin{equation}
	y^{\prime\prime}-x y=0,
	\end{equation}
	
	uitrekenen met behulp van machtreeksen. Dus we stellen een oplossing voor van de vorm

	\begin{equation}
	y=\sum_{n=0}^{\infty} a_{n} x^{n}
	\end{equation}
	
	waarmee we volgende vergelijking krijgen
	
	\begin{equation}
	\begin{aligned}
	&\sum_{n=2}^{\infty} n(n-1) a_{n} x^{n-2}-\sum_{n=0}^{\infty} a_{n} x^{n+1}=0 \\
	&2a_{2} + \sum_{n=1}^{\infty} \left[ (n+2)(n+1) a_{n+2}-a_{n-1} \right]x^{n}=0  \\
	\end{aligned}
	\end{equation}
	
	
\newpage
\bibliographystyle{unsrt}
\bibliography{biblio}

\end{document}
